package eu.epfc.pocketmovie

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout

class MainActivity : AppCompatActivity() {


    private lateinit var viewPager : ViewPager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Passe l'adapter au viewPager

        viewPager = findViewById(R.id.pager)
        val pagerAdapter = PagerAdapter(supportFragmentManager)
        viewPager.adapter = pagerAdapter

        // Passe le viewPager au TabLayout

        val tabLayout : TabLayout = findViewById(R.id.tab_layout)
        tabLayout.setupWithViewPager(viewPager)

    }
}