package eu.epfc.pocketmovie

import android.content.Intent
import android.graphics.pdf.PdfDocument
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.size
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import eu.epfc.pocketmovie.data.MovieRepository

// Fragment de l'onglet Recent Movie
class MovieFragment : Fragment(), MoviesAdapter.ListItemClickListener, MovieRepository.DataUIListener {

    private var recyclerView : RecyclerView? = null

    private lateinit var myAdapter : MoviesAdapter

    private lateinit var myLayoutManager: LinearLayoutManager

    private var position : Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        position = savedInstanceState?.getInt("position", 0) ?: 0
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movie, container, false)
    }

    override fun onStart() {
        super.onStart()

        // Passe l'adapter à la recycler view

        recyclerView = view?.findViewById(R.id.list_movie)
        myAdapter = MoviesAdapter(this)

        // Donne l'info à l'adapter que nous sommes dans le movieFragment

        myAdapter.isMoviesFragment = true

        recyclerView?.adapter = myAdapter

        // Passe le LayoutManager au recycler view
        myLayoutManager = LinearLayoutManager(activity)
        recyclerView?.layoutManager = myLayoutManager

        // Scroll à la position du dernier click
        recyclerView?.scrollToPosition(position)


        // Passe la référence à l'activité dans le MovieRepository

        MovieRepository.instance.observe(this)

        /* Je lance la génération de la liste de film (par requête http) via la fonction generateRecentMovies,
        le check de isFirstLaunch permet de ne pas multiplier plusieurs fois les pages */

        if (MovieRepository.instance.isMovieFragmentFirstLaunch) {
            MovieRepository.instance.generateRecentMovies(activity?.applicationContext ?: MainActivity())
            MovieRepository.instance.isMovieFragmentFirstLaunch = false
        } else updateUI()

    }

    override fun onListItemClick(clickedItemIndex: Int) {
        // Permet de passer à la page suivante
        if (clickedItemIndex == MovieRepository.instance.movies.size) {
            if (recyclerView?.size != 1) {
                MovieRepository.instance.page += 1
            }
            MovieRepository.instance.generateRecentMovies(activity?.applicationContext ?: MainActivity())
        }
        // Permet de passer à l'activité Detail
        else {
            val intent = Intent(activity, DetailActivity::class.java)
            // donnée à ajouter à l'intent pour le passer à la DetailActivity
            val movieList = MovieRepository.instance.movies
            val movie : Int = movieList.get(clickedItemIndex).id
            intent.putExtra("movieID", movie)
            startActivity(intent)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        // Permet de stocker la position de l'item dans les savedInstanceState
        position = myLayoutManager.findFirstVisibleItemPosition()
        outState.putInt("position", position)
    }

    // Override la fonction qui se trouve dans MovieRepository qui permet d'update l'interface

    override fun updateUI() {

        // En cas d'erreur, on génère un toast
        if (MovieRepository.instance.httpFail) {
            Toast.makeText(activity, "Erreur de connexion à Internet", Toast.LENGTH_SHORT).show()
        }

        // Dans tous les cas, on affiche les films
        val movieList = MovieRepository.instance.movies

        myAdapter.movies = movieList

    }
}
