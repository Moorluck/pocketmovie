package eu.epfc.pocketmovie

import androidx.room.Entity
import androidx.room.PrimaryKey

// classe movie inséré dans une base de donnée
// le genre est nullable car on l'initialisera dans la detailActivity

@Entity(tableName = "movies")
class Movie (id : Int, val title : String, val rating : String, val date : String,
             var genre : String? = null, val resume : String, val posterImageUrl : String,
             val backdropImageUrl : String) {

    @PrimaryKey
    val id = id

}