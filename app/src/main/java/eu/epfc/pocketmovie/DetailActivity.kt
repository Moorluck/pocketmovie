package eu.epfc.pocketmovie

import android.annotation.SuppressLint
import android.app.ActionBar.DISPLAY_HOME_AS_UP
import android.app.ActionBar.DISPLAY_SHOW_HOME
import android.content.Intent
import android.content.res.Configuration
import android.media.Image
import android.net.Uri
import android.opengl.Visibility
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.constraintlayout.widget.ConstraintLayout
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import eu.epfc.pocketmovie.data.MovieRepository
import eu.epfc.pocketmovie.data.PocketRepository
import java.lang.Exception

class DetailActivity : AppCompatActivity(), MovieRepository.DataUIListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        // Permet de faire apparaître le bouton "back" dans l'Action Bar (en ajoutant l'activité mère dans l'android manifest)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        // Génère la référence à l'activité dans le MovieRepository
        MovieRepository.instance.observe(this)

        // récupère et génère les détails du movie
        val movieID = intent.getIntExtra("movieID", 0)
        MovieRepository.instance.movieID = movieID
        MovieRepository.instance.generateMovieDetails(this)

        // checkbox
        val checkBox : CheckBox = findViewById(R.id.pocket_checkboxe)

        /* Permet de check si il faut cocher ou non la checkbox en fonction des ID des films
        compris dans la DB */
        val listOfID = mutableListOf<Int>()

        for (element in PocketRepository.instance.movies) {
            val id = element.id
            listOfID.add(id)
        }

        checkBox.isChecked = listOfID.contains(movieID) // Booléen

        val videoBtn : Button = findViewById(R.id.video_btn)

        // Permet de démarrer le HttpReceiver qui recevra l'ID youtube
        videoBtn.setOnClickListener() {
            MovieRepository.instance.generateVideoURL(this, movieID)
        }


        // Permet d'ajouter ou de supprimer un movie de la DB selon qu'on check ou pas la checkbox
        checkBox.setOnClickListener() {
            if (!checkBox.isChecked) {
                PocketRepository.instance.deleteMovieFromPocket(this, movieID)
            } else {
                PocketRepository.instance.addMovieToPocket(this, MovieRepository.instance.movie)
            }
        }
    }

    override fun onStart() {
        super.onStart()

        // Change la hauteur de l'image selon l'orientation

        val backdropImageView : ImageView = findViewById(R.id.backdrop_image)

        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            backdropImageView.layoutParams.height = 750
        } else {
            backdropImageView.layoutParams.height = 600
        }
    }


    // Permet à la fleche en haut à gauche de se comporter comme le back Button en bas
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        onBackPressed()
        return true
    }

    override fun updateUI() {

        val movieTitle : TextView = findViewById(R.id.movie_title)
        val movieDate : TextView = findViewById(R.id.movie_date)
        val movieGenre : TextView = findViewById(R.id.movie_genre)
        val movieRating : TextView = findViewById(R.id.movie_rating_detail)
        val movieResume : TextView = findViewById(R.id.movie_resume)
        val backdropImageView : ImageView = findViewById(R.id.backdrop_image)
        val progressBar : ProgressBar = findViewById(R.id.progressBar)

        // Si on vient de cliquer sur le bouton qui lance youtube
        if (MovieRepository.instance.isLookingForVideoURL) {
            MovieRepository.instance.isLookingForVideoURL = false

            if (MovieRepository.instance.videoURL == "false") {
                Toast.makeText(this, "Impossible d'accéder à la vidéo Youtube",
                    Toast.LENGTH_SHORT).show()
            }
            else {
                val youtubeId = MovieRepository.instance.videoURL
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$youtubeId"))
                startActivity(intent)
            }

        // Si la requête n'a pas fonctionné, on enlève tout et on laisse l'image par défaut
        } else if (MovieRepository.instance.httpFail) {
            Toast.makeText(this, "Erreur de connexion à Internet",
                    Toast.LENGTH_SHORT).show()

            movieTitle.text = ""
            movieDate.text = ""
            movieGenre.text = ""
            movieRating.text = ""
            movieResume.text = ""
        }

        // Situation normal, on update les données et on charge l'image
        else {
            // update l'interface par rapport aux infos de MovieRepository
            val imageUrl = "https://image.tmdb.org/t/p/w500"

            val movie = MovieRepository.instance.movie

            movieTitle.text = movie?.title
            movieDate.text = movie?.date
            movieGenre.text = movie?.genre.toString().replace("[", "").replace("]", "")
            movieRating.text = movie?.rating
            movieResume.text = movie?.resume


            Picasso.get()
                .load(imageUrl + movie?.backdropImageUrl)
                .fit()
                .centerCrop()
                .into(backdropImageView, object : Callback {
                    override fun onSuccess() {
                        progressBar.visibility = View.GONE
                    }

                    override fun onError(e: Exception?) {
                        progressBar.visibility = View.VISIBLE
                    }
                })
        }
    }
}