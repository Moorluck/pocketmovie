package eu.epfc.pocketmovie

import android.graphics.Movie
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter

class PagerAdapter(fragmentManager: FragmentManager) :
    FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {

        // Fragment à afficher selon l'onglet sélectionné (position)

        return when (position) {
            0 -> MovieFragment()
            1 -> PocketFragment()
            else -> MovieFragment()
        }
    }

    override fun getCount(): Int {
        //nombre d'onglet
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        // nom des onglets
        return when (position) {
            0 -> "RECENT MOVIES"
            1 -> "POCKET"
            else -> "ERROR"
        }

    }

}