package eu.epfc.pocketmovie

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import eu.epfc.pocketmovie.data.MovieRepository
import eu.epfc.pocketmovie.data.PocketRepository

// Fragment de l'onglet Pocket
class PocketFragment : Fragment(), MoviesAdapter.ListItemClickListener, PocketRepository.DataUIListener {

    private lateinit var myAdapter: MoviesAdapter

    private lateinit var myLayoutManager : LinearLayoutManager

    private var position : Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        position = savedInstanceState?.getInt("position", 0) ?: 0
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pocket, container, false)
    }

    override fun onStart() {
        super.onStart()

        // Passe l'adapter à la recycler view
        val recyclerView : RecyclerView? =view?.findViewById(R.id.list_movie)
        myAdapter = MoviesAdapter(this)

        // Donne l'info à l'adapter que nous sommes dans le PocketFragment

        myAdapter.isMoviesFragment = false

        recyclerView?.adapter = myAdapter

        // Passe le LayoutManager au recycler view
        myLayoutManager = LinearLayoutManager(activity)
        recyclerView?.layoutManager = myLayoutManager

        // Scroll à la dernière position

        recyclerView?.scrollToPosition(position)

        PocketRepository.instance.observe(this)

        PocketRepository.instance.generatePocketMovie(activity?.applicationContext ?: MainActivity())

    }

    override fun onListItemClick(clickedItemIndex: Int) {
        val intent = Intent(activity, DetailActivity::class.java)
        // donnée à ajouter à l'intent pour le passer à la DetailActivity
        val movieList = PocketRepository.instance.movies
        val movie : Int = movieList.get(clickedItemIndex).id ?: 0
        intent.putExtra("movieID", movie)
        startActivity(intent)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        // Permet de stocker la position de l'item dans les savedInstanceState
        super.onSaveInstanceState(outState)
        position = myLayoutManager.findFirstVisibleItemPosition()
        outState.putInt("position", position)
    }


    override fun updateUI() {

        val movies : List<Movie> = PocketRepository.instance.movies

        myAdapter.movies = movies

    }
}