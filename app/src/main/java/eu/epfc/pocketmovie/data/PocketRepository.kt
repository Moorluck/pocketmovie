package eu.epfc.pocketmovie.data

import android.content.Context
import eu.epfc.pocketmovie.Movie
import java.lang.ref.WeakReference

/* La classe collecte les infos relatif à la DB, ce qui permettra d'afficher la liste des film favori
(utile uniquement au PocketFragment). Fonctionne sur le même principe que MovieRepository*/

class PocketRepository {

    var movies : List<Movie> = mutableListOf()

    private var dataUIListener : WeakReference<PocketRepository.DataUIListener>? = null

    interface DataUIListener {
        fun updateUI()
    }

    // Référence à l'activité
    fun observe(dataUIListener: PocketRepository.DataUIListener) {
        this.dataUIListener = WeakReference(dataUIListener)
    }

    // Génère les films
    fun generatePocketMovie(context: Context) {
        val movieDAO = MovieDatabase.getInstance(context).movieDAO()
        movies = movieDAO.getAllMovies()

        val myDataUIListener = dataUIListener
        if (myDataUIListener != null) {
            myDataUIListener.get()?.updateUI()
        }
    }

    // Ajoute un film à la DB
    fun addMovieToPocket(context: Context, movie : Movie?) {
        val movieDAO = MovieDatabase.getInstance(context).movieDAO()
        movieDAO.insertMovie(movie)

    }

    // Supprime un film de la DB
    fun deleteMovieFromPocket(context: Context, movieID: Int?) {
        val movieDAO = MovieDatabase.getInstance(context).movieDAO()
        movieDAO.removeMovie(movieID)

    }

    companion object {
        val instance = PocketRepository()
    }
}