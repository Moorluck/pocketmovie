package eu.epfc.pocketmovie.data

import androidx.room.*
import eu.epfc.pocketmovie.Movie

// Toute les fonctions qu'on peut appeller pour gérer la base de donnée et récupérer les données de la DB

@Dao
interface MovieDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovie(movie : Movie?)

    @Query("SELECT * FROM movies")
    fun getAllMovies() : List<Movie>

    @Query("DELETE FROM movies WHERE id = :movieID")
    fun removeMovie(movieID : Int?)

}