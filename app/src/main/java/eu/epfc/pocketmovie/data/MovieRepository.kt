package eu.epfc.pocketmovie.data

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.util.Log
import eu.epfc.pocketmovie.HttpRequestTask
import eu.epfc.pocketmovie.Movie
import org.json.JSONException
import org.json.JSONObject
import java.lang.ref.WeakReference

// La classe collecte toutes les informations récupéré par requête HTTP (autant pour les détails
// des films que pour la liste des films)

class MovieRepository {

    // API Key
    private val apiKey : String = "ea2dcee690e0af8bb04f37aa35b75075"

    /* Booléenne pour savoir si le reciever est initialisé et pour savoir si la dernière requête
    a fonctionné ou pas */
    var httpReceiverInitialized = false
    var httpFail = false

    /* Booléenne pour savoir si c'est le premier lancement de MovieFragment (sinon il génère plusieurs
    fois la même liste) */
    var isMovieFragmentFirstLaunch = true

    // Page
    var page : Int = 1

    // Variable qui contiendra la liste de film généré par requête http
    var movies : MutableList<Movie> = mutableListOf()

    // Url et ID par rapport aux détails d'un film
    var movie : Movie? = null
    var movieID : Int? = null

    // Booléenne pour savoir si on est dans la DetailActivity
    private var isDetailActivity = false

    // Booléenne concernant le lien vers youtube
    var isLookingForVideoURL = false
    var videoURL : String = ""

    // Référence aux MovieFragment ou à la DetailActivity (sans faire appel à ceux-ci)
    private var dataUIListener : WeakReference<DataUIListener>? = null

    // Ce qu'il faut override dans le MovieFragment ou la DetailActivity qui permettra d'update l'interface
    interface DataUIListener {
        fun updateUI()
    }

    // Ce qui gère le broadcast receiver
    private inner class HttpReceiver: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.action == "httpRequestComplete") {

                val response = intent.getStringExtra("responseString")

                httpFail = false

                if (response != null) {
                    if (isLookingForVideoURL) {
                        // Si on reçoit le JSON qui nous permet d'avoir le lien youtube
                        videoURL = MovieRepository.instance.parseMovieVideoResponse(response)
                    }

                    else if (!isDetailActivity) {
                        // Si on se trouve dans le MovieFragment, on parse la liste de film
                        val newMovies : MutableList<Movie> = MovieRepository.instance.parseMovieResponse(response)
                        movies.addAll(newMovies)
                    }

                    else if (isDetailActivity) {
                        // Si on se trouve dans la DetailActivity, on parse les détails d'un film
                        movie = MovieRepository.instance.parseMovieDetailResponse(response)
                    }
                }
            }

            else if (intent?.action == "httpRequestError") {
                // Si l'erreur a lieu quand on clique sur NextPage, on réinitialise la varialbe page
                httpFail = true

                if (page > 1 && !isDetailActivity) {
                    page -= 1
                }
            }

            // On update l'interface selon ce qu'on a
            val myDataUIListener = dataUIListener
            if (myDataUIListener != null) {
                myDataUIListener.get()?.updateUI()
            }
        }
    }

    // Fonction qui démarre l'HTTPReceiver
    private fun initHttpReceiverWithContext(context: Context) {
        val intentFilter = IntentFilter()
        intentFilter.addAction("httpRequestComplete")
        intentFilter.addAction("httpRequestError")
        val httpReceiver = HttpReceiver()
        context.applicationContext.registerReceiver(httpReceiver, intentFilter)
    }

    // Permet de faire une WeakReference au MovieFragment ou à la DetailActivity
    fun observe(dataUIListener: DataUIListener) {
        this.dataUIListener = WeakReference(dataUIListener)
    }

    // Fonction qui permettra au MovieFragment de générer les données
    fun generateRecentMovies(context: Context) {

        if(!httpReceiverInitialized){
            initHttpReceiverWithContext(context.applicationContext)
            httpReceiverInitialized = true
        }

        isDetailActivity = false

        val urlMovies : String = "https://api.themoviedb.org/3/movie/popular?page=$page&api_key=$apiKey"

        val requestTask = HttpRequestTask(urlMovies,context.applicationContext)
        val requestThread = Thread(requestTask)
        requestThread.start()
    }

    // Permettra à la DetailActivity de généré les détails des film

    fun generateMovieDetails(context: Context) {

        isDetailActivity = true

        val urlMovieDetail : String = "https://api.themoviedb.org/3/movie/${movieID.toString()}?api_key=$apiKey"

        val requestTask = HttpRequestTask(urlMovieDetail,context.applicationContext)
        val requestThread = Thread(requestTask)
        requestThread.start()
    }

    // génère l'ID de la vidéo youtube
    fun generateVideoURL(context: Context, movieID : Int) {

        isLookingForVideoURL = true

        val urlMovieVideoDetail : String = "https://api.themoviedb.org/3/movie/${movieID}/videos?api_key=$apiKey"

        val requestTask = HttpRequestTask(urlMovieVideoDetail,context.applicationContext)
        val requestThread = Thread(requestTask)
        requestThread.start()
    }

    // Parse le JSON et en fait une liste de film pour le MovieFragment
    private fun parseMovieResponse (jsonString : String) : ArrayList<Movie> {

        val listOfRecentMovie= ArrayList<Movie>()

        try {
            val jsonObject = JSONObject(jsonString)
            val jsonMovies = jsonObject.getJSONArray("results")

            for (i in 0 until jsonMovies.length()) {

                val jsonMovie = jsonMovies.getJSONObject(i)

                val id = jsonMovie.getString("id")
                val title = jsonMovie.getString("title")
                val rating = jsonMovie.getString("vote_average")
                val date = jsonMovie.getString("release_date")
                val resume = jsonMovie.getString("overview")
                val posterImageURL = jsonMovie.getString("poster_path")
                val backdropImageURL = jsonMovie.getString("backdrop_path")

                val newMovie = Movie(id = id.toInt(), title = title, rating = rating, date = date,
                        resume = resume, posterImageUrl = posterImageURL,
                        backdropImageUrl = backdropImageURL)

                listOfRecentMovie.add(newMovie)
            }
        } catch (e: JSONException){
            Log.e("MovieRepository", "can't parse json string correctly")
            e.printStackTrace()
        }

        return listOfRecentMovie
    }

    // Parse le JSON reçu pour en faire un objet Movie pour la DetailActivity

    private fun parseMovieDetailResponse (jsonString: String) : Movie? {

        val jsonMovie = JSONObject(jsonString)

        val id = jsonMovie.getString("id")
        val title = jsonMovie.getString("title")
        val rating = jsonMovie.getString("vote_average")
        val date = jsonMovie.getString("release_date")

        val jsonGenre = jsonMovie.getJSONArray("genres")
        val genre : MutableList<String?> = mutableListOf<String?>()
        for (i in 0 until jsonGenre.length()) {
            val genreJSONObject = jsonGenre.getJSONObject(i)
            val genreName = genreJSONObject.getString("name")
            genre.add(genreName)
        }

        val resume = jsonMovie.getString("overview")
        val posterImageURL = jsonMovie.getString("poster_path")
        val backdropImageURL = jsonMovie.getString("backdrop_path")

        val newMovie = Movie(id = id.toInt(), title = title, rating = rating, date = date,
                genre = genre.toString().replace("[", "").replace("]", ""),
                resume = resume, posterImageUrl = posterImageURL,
                backdropImageUrl = backdropImageURL)

        return newMovie
    }

    // Parse le JSON reçu pour générer l'URL youtube

    fun parseMovieVideoResponse (jsonString: String) : String {

        val jsonObject = JSONObject(jsonString)
        val jsonMovieVideoArray = jsonObject.getJSONArray("results")

        var url = ""

        url = try {
            val jsonMovieVideo = jsonMovieVideoArray.getJSONObject(0)
            jsonMovieVideo.getString("key")
        } catch (e : JSONException) {
            Log.d("erreur", "impossible de parse")
            "false"
        }

        return url
    }

    // Permet à la classe d'être un singleton
    companion object {
        val instance = MovieRepository()
    }
}