package eu.epfc.pocketmovie.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import eu.epfc.pocketmovie.Movie

// classe qui permettra de manipuler la base de donner avec les fonction de MovieDAO

@Database(entities = [Movie::class], version = 2, exportSchema = false)
abstract class MovieDatabase : RoomDatabase() {

    companion object {

        private const val DATABASE_NAME = "PocketMovieDataBase"
        private var sInstance : MovieDatabase? = null

        fun getInstance(context: Context) : MovieDatabase {
            if (sInstance == null) {
                val dbBuilder = Room.databaseBuilder(
                    context.applicationContext,
                    MovieDatabase::class.java,
                    DATABASE_NAME
                )
                dbBuilder.allowMainThreadQueries()
                sInstance = dbBuilder.build()
            }

            return sInstance!!
        }
    }

    abstract fun movieDAO() : MovieDAO


}