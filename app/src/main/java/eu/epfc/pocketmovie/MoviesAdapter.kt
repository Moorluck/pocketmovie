package eu.epfc.pocketmovie

import android.media.Image
import android.opengl.Visibility
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import java.lang.Exception

// Adapter de la recycler view
class MoviesAdapter (val listItemClickListener : ListItemClickListener) : RecyclerView.Adapter<MoviesAdapter.MoviesViewHolder>() {

    // liste des films à afficher dans les recycler view

    var movies : List<Movie>? = null
    set (value) {
        field = value
        notifyDataSetChanged()
    }

    var imageUrl = "https://image.tmdb.org/t/p/w500"

    var isMoviesFragment : Boolean = true

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesAdapter.MoviesViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val movieView = layoutInflater.inflate(R.layout.item_movie, parent, false)

        return MoviesViewHolder(movieView)
    }

    // nombre d'item dans la liste
    override fun getItemCount(): Int {
        return when (isMoviesFragment) {
            true -> (movies?.size ?:0) + 1
            false -> movies?.size ?: 0
        }
    }

    // ce qui permet d'afficher les films
    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {

        val movieList = movies

        val itemViewGroup = holder.itemView as ViewGroup

        val titleTextView : TextView = itemViewGroup.findViewById(R.id.movie_name)
        val ratingTextView : TextView = itemViewGroup.findViewById(R.id.movie_rating)
        val posterView : ImageView = itemViewGroup.findViewById(R.id.poster_image)
        val nextPageView : TextView = itemViewGroup.findViewById(R.id.next_page)
        val progressBar : ProgressBar = itemViewGroup.findViewById(R.id.progressBar2)

        if (movieList != null) {
            // Pour générer tous les items de la recycler view sauf le dernier
            if (position < (movieList.size)) {

                titleTextView.visibility = View.VISIBLE
                ratingTextView.visibility = View.VISIBLE

                posterView.visibility = View.VISIBLE
                nextPageView.visibility = View.INVISIBLE

                val movie = movieList[position]
                titleTextView.text = movie.title
                ratingTextView.text = "Rating : ${movie.rating}"

                Picasso.get()
                    .load(imageUrl + movie.posterImageUrl)
                    .into(posterView, object : Callback {
                        override fun onSuccess() {
                            progressBar.visibility = View.GONE
                        }

                        override fun onError(e: Exception?) {
                            progressBar.visibility = View.VISIBLE
                        }
                    })



            // Pour générer le dernière item de la recycler view (dans le cas
            } else if (position == movieList.size) {

                titleTextView.visibility = View.INVISIBLE
                ratingTextView.visibility = View.INVISIBLE

                posterView.visibility = View.INVISIBLE
                nextPageView.visibility = View.VISIBLE

                progressBar.visibility = View.INVISIBLE

                if (movieList.isEmpty()) {
                    nextPageView.text = "Refresh"
                } else {
                    nextPageView.text = "Next page"
                }

            }
        }


    }

    // interface qui permet de redéfinir ailleurs la fonction appellé quand on clique sur un item
    // (dans MovieFragment et PocketFragment)
    interface ListItemClickListener {
        fun onListItemClick(clickedItemIndex : Int)
    }

    // ViewHolder
    inner class MoviesViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val clickedPosition = this.adapterPosition
            this@MoviesAdapter.listItemClickListener.onListItemClick(clickedPosition)
        }
    }
}